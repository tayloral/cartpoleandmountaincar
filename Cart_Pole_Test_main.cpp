/* 
 * File:   test_main.cpp
 * Author: Adam
 *
 * Created on 26 March 2015, 10:19
 */

#include <cstdlib>
#include <iostream>
#include "CartPole.h"
#include "DWL/include/DWLAgent.h"
#include "CartPoleAgent.h"

using namespace std;


bool usingMergeAndAdapt = true;

/*
 * 
 */
int Cart_main(int argc, char** argv)//learning_test_
{
    cout << "cart pole test\n";
    double explore[11 * 11];
    int index = -1;
    double exploite[11 * 11];
    for (double alpha = 0.0; alpha <= 1; alpha = alpha + .1)
    {
        cout << alpha << "-";
        cout.flush();
        for (double gamma = 0.0; gamma <= 1; gamma = gamma + .1)
        {
            cout << gamma << ",";
            cout.flush();
            index++;
            double thisExplore = 0;
            double thisExploite = 0;
            for (int times = 0; times < 100; times++)
            {
                //create and init an agent
                CartPole* cp = new CartPole();
                CartPoleAgent* agent = new CartPoleAgent("testAgent");
                agent->changeAlphaGamma(alpha, gamma);
                agent->changeActionSelectionTemperature(1000);
                agent->setUsingTransferLearning(false);
                agent->manageLearning(true, !true);
                agent->chooseActionSelectionMethod(false, false, !false);
                //storage space
                int searchPoleCurrentAction = -10;
                int searchThisTimeCounter = 0;
                int searchNumberOfRuns = 100;
                int searchTotalTimeCounter = 0;
                int searchMax = -1;
                int heartbeat = 0;
                for (int a = 0; a < searchNumberOfRuns; a++)
                {//run a few times                    
                    while (cp->getCurrentState() != "-1" && heartbeat < 5000)
                    {//while vertical
                        searchPoleCurrentAction = atoi(agent->nominate().c_str());
                        cp->executeAction(searchPoleCurrentAction);
                        agent->updateLocal(cp->getCurrentState());
                        agent->finishRun();
                        if (usingMergeAndAdapt == true)
                        {
                            agent->adaptAndReconfigure();
                        }
                        searchThisTimeCounter++;
                        searchTotalTimeCounter++; //the average counter
                        heartbeat++;
                    }
                    //std::cout << "Run " << a << " lasted for " << searchThisTimeCounter << " steps\n";

                    agent->changeActionSelectionTemperature(1000 - (a * 1000 / searchNumberOfRuns)); //make a more directed search
                    cp->reset();
                    if (searchThisTimeCounter > searchMax)
                    {//if better than the best
                        searchMax = searchThisTimeCounter;
                    }
                    searchThisTimeCounter = 0;
                }
                //std::cout << "Ended Exploration " << searchNumberOfRuns << " runs averaging " << ((double) searchTotalTimeCounter / searchNumberOfRuns) << " steps with a max of " << searchMax << " steps\n";
                thisExplore += ((double) searchTotalTimeCounter / searchNumberOfRuns);
                //exploitation
                agent->changeActionSelectionTemperature(1);
                int usePoleCurrentAction = -10;
                int useThisTimeCounter = 0;
                int useNumberOfRuns = 10;
                int useTotalTimeCounter = 0;
                int useMax = -1;
                heartbeat = 0;
                for (int a = 0; a < useNumberOfRuns; a++)
                {//run a few times
                    while (cp->getCurrentState() != "-1" && heartbeat < 5000)
                    {//while vertical
                        usePoleCurrentAction = atoi(agent->nominate().c_str());
                        cp->executeAction(usePoleCurrentAction);
                        agent->updateLocal(cp->getCurrentState());
                        agent->finishRun();
                        if (usingMergeAndAdapt == true)
                        {
                            agent->adaptAndReconfigure();
                        }
                        useThisTimeCounter++;
                        useTotalTimeCounter++; //the average counter
                        heartbeat++;
                    }
                    // std::cout << "Run " << a << " lasted for " << useThisTimeCounter << " steps\n";
                    cp->reset();
                    if (useThisTimeCounter > useMax)
                    {//if better than the best
                        useMax = useThisTimeCounter;
                    }
                    useThisTimeCounter = 0;
                }
                //std::cout << "Ended Exploitation " << useNumberOfRuns << " runs averaging " << ((double) useTotalTimeCounter / useNumberOfRuns) << " steps with a max of " << useMax << " steps\n";
                thisExploite += ((double) useTotalTimeCounter / useNumberOfRuns);
            }
            explore[index] = thisExplore / 100;
            exploite[index] = thisExploite / 100;
        }
        cout << endl;
    }
    cout << "explore:\n";
    for (int a = 0; a < 11 * 11; a++)
    {
        cout << explore[a] << ",";
    }
    cout << "\nexploite:\n";
    for (int a = 0; a < 11 * 11; a++)
    {
        cout << exploite[a] << ",";
    }
    return 0;


}