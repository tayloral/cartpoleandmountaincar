/* 
 * File:   test_main.cpp
 * Author: Adam
 *
 * Created on 26 March 2015, 10:19
 */

#include <cstdlib>
#include <iostream>
#include "MtCar.h"
#include "CartPole.h"
#include "DWL/include/DWLAgent.h"
#include "MtCarAgent.h"
#include "CartPoleAgent.h"
#include <sstream>
#include <fstream>

using namespace std;
//name
std::string outputFileTag = "H-Mapping-PTL-MC2CP-100-4"; //down
std::string outputFilePrefix = "";
//control parameters
int bigLoop = 10;
int searchNumberOfRuns = 100;
int useNumberOfRuns = 10;
int maxTime = 5000;
bool loopBreak = false;
bool targetLearn = !false;
bool useTL = true;
bool useSearchMapping = false;
bool mappingAnts = false;
bool mappingFeedback = !false;
bool usingMergeAndAdapt = !true;
//variables
int sourceBetterMax = 0;
int sourceBetterAve = 0;
std::vector<int> sourceSearch;
std::vector<int> targetSearch;
std::vector<int> sourceUse;
std::vector<int> targetUse;

//functions
void TLmainRunJava(std::string location, std::string tag, std::string from);
void TLmainWriteData(std::string fileName, std::string tag);

/*
 * 
 */
int main(int argc, char** argv)
{
    for (int mainLoopCount = 0; mainLoopCount < bigLoop; mainLoopCount++)
    {
        std::cout << "MC2CP=================================Round " << mainLoopCount << "=================================\n";
        //create and init an agent source
        MtCar* sourceMC = new MtCar();
        MtCarAgent* sourceAgent = new MtCarAgent("sourceAgent");
        sourceAgent->changeAlphaGamma(1, .1);
        sourceAgent->changeActionSelectionTemperature(1000);
        sourceAgent->manageLearning(true, !true);
        sourceAgent->chooseActionSelectionMethod(false, false, !false);
        sourceAgent->setUsingTransferLearning(useTL);
        //create and init an agent target
        CartPole* targetCP = new CartPole();
        CartPoleAgent* targetAgent = new CartPoleAgent("targetAgent");
        targetAgent->changeAlphaGamma(.9, .2);
        targetAgent->changeActionSelectionTemperature(1000);
        targetAgent->manageLearning(targetLearn, !true);
        targetAgent->chooseActionSelectionMethod(false, false, !false);
        targetAgent->setUsingTransferLearning(useTL);

        //make a mapping
        TransferConfiguration sourceConfig, targetConfig;
        sourceConfig.amountToTransfer = 1;
        // sourceConfig.mergeParam = 5;
        //sourceConfig.merge = MergeType::adaptive;
        sourceConfig.selection = SelectionType::best;
        sourceConfig.environment.fastChange = false;
        sourceConfig.environment.iChanged = false;
        sourceConfig.environment.iLearned = false;
        sourceConfig.environment.neighbourChange = false;
        sourceConfig.environment.processedILearned = false;
        sourceConfig.environment.processedSharpChnageHappened = false;
        sourceConfig.environment.sharpChnageHappened = false;
        sourceConfig.environment.slowChange = false;
        //targetConfig.amountToTransfer = 1;
        targetConfig.mergeParam = 4;
        targetConfig.merge = MergeType::adaptive;
        // targetConfig.selection = SelectionType::best;
        targetConfig.environment.fastChange = false;
        targetConfig.environment.iChanged = false;
        targetConfig.environment.iLearned = false;
        targetConfig.environment.neighbourChange = false;
        targetConfig.environment.processedILearned = false;
        targetConfig.environment.processedSharpChnageHappened = false;
        targetConfig.environment.sharpChnageHappened = false;
        targetConfig.environment.slowChange = false;
        sourceAgent->setConfigParams(sourceConfig, 50, 30, 180, 40);
        targetAgent->setConfigParams(targetConfig, 50, 30, 180, 40);
        if (mappingAnts)
        {
            sourceAgent->addMapping("sourceAgent+MtCar", "targetAgent+CartPole", (*sourceAgent->getLocalQTables().begin()), (*targetAgent->getLocalQTables().begin()), new RewardMtCar(), new RewardCartPole());
            sourceAgent->printMappings("Begin", 1, outputFileTag);
        }
        else if (useTL)
        {
            if (useSearchMapping)
            {

                sourceAgent->addSearchMapping("sourceAgent+MtCar", "targetAgent+CartPole", (*sourceAgent->getLocalQTables().begin()), (*targetAgent->getLocalQTables().begin()));
            }
            else
            {
                sourceAgent->addMapping("sourceAgent+MtCar", "targetAgent+CartPole", (*sourceAgent->getLocalQTables().begin()), (*targetAgent->getLocalQTables().begin()), !mappingFeedback);
                sourceAgent->printMappings("Begin", 1, outputFileTag);
            }

        }

        //storage space
        //source
        int searchSourcePoleCurrentAction = -10;
        int searchSourceThisTimeCounter = 0;
        int searchSourceTotalTimeCounter = 0;
        int searchSourceMax = -1;
        //target
        int searchTargetPoleCurrentAction = -10;
        int searchTargetThisTimeCounter = 0;
        int searchTargetTotalTimeCounter = 0;
        int searchTargetMax = -1;
        for (int a = 0; a < searchNumberOfRuns; a++)
        {//run a few times
            bool sourceDone = false;
            while ((loopBreak == false)&&((sourceDone == false) || (targetCP->getCurrentState() != "-1")))
            {//while one is vertical 
                //source
                if (sourceDone != true)
                {
                    searchSourcePoleCurrentAction = atoi(sourceAgent->nominate().c_str());
                    sourceDone = sourceMC->executeAction(searchSourcePoleCurrentAction);
                    sourceAgent->updateLocal(sourceMC->getState());
                    sourceAgent->finishRun();
                    searchSourceThisTimeCounter++;
                    searchSourceTotalTimeCounter++; //the average counter
                    if (usingMergeAndAdapt == true)
                    {
                        sourceAgent->adaptAndReconfigure();
                    }
                }
                //target
                if (targetCP->getCurrentState() != "-1")
                {
                    searchTargetPoleCurrentAction = atoi(targetAgent->nominate().c_str());
                    targetCP->executeAction(searchTargetPoleCurrentAction);
                    targetAgent->updateLocal(targetCP->getCurrentState());
                    targetAgent->finishRun();
                    searchTargetThisTimeCounter++;
                    searchTargetTotalTimeCounter++; //the average counter
                    if (usingMergeAndAdapt == true)
                    {
                        targetAgent->adaptAndReconfigure();
                    }
                }
                //do the transfer
                if (useTL)
                {
                    if (mappingAnts)
                    {
                        sourceAgent->updateLearnedMappingFromAnts();
                    }
                    //finished runs now transfer
                    std::string transfer = sourceAgent->transferToAllFromAll();
                    targetAgent->readTransferedInfoIn(transfer);
                    //now feedback
                    std::string feedback = targetAgent->sendFeedback();
                    sourceAgent->sendFeedback(); //just to clear vector
                    if (mappingFeedback)
                    {
                        sourceAgent->recieveFeedback(feedback);
                    }
                    //lern from feedback
                    if (mappingFeedback)
                    {
                        sourceAgent->updateLearnedMappingFromTarget();
                    }
                }
                if ((searchTargetThisTimeCounter > maxTime) || (searchSourceThisTimeCounter > maxTime))
                {//if we've been alive a very long time
                    loopBreak = true;
                    std::cout << "had to break because searchTargetThisTimeCounter= " << searchTargetThisTimeCounter << " and searchSourceThisTimeCounter= " << searchSourceThisTimeCounter << "\n";
                }
            }
            //std::cout << "Source-Run " << a << " lasted for " << searchSourceThisTimeCounter << " steps\n";
            //std::cout << "Target-Run " << a << " lasted for " << searchTargetThisTimeCounter << " steps\n";

            sourceAgent->changeActionSelectionTemperature(1000 - (a * 1000 / searchNumberOfRuns)); //make a more directed search
            targetAgent->changeActionSelectionTemperature(1000 - (a * 1000 / searchNumberOfRuns)); //make a more directed search
            sourceMC->reset(false);
            targetCP->reset();
            loopBreak = false;
            if (searchSourceThisTimeCounter > searchSourceMax)
            {//if better than the best
                searchSourceMax = searchSourceThisTimeCounter;
            }
            sourceSearch.push_back(searchSourceThisTimeCounter);
            searchSourceThisTimeCounter = 0;
            if (searchTargetThisTimeCounter > searchTargetMax)
            {//if better than the best
                searchTargetMax = searchTargetThisTimeCounter;
            }
            targetSearch.push_back(searchTargetThisTimeCounter);
            searchTargetThisTimeCounter = 0;

        }
        std::stringstream ss;
        ss << "sourceSearchReward";
        sourceAgent->printReward(ss.str(), outputFileTag); //write this file name with the tag
        ss.str("");
        ss << "targetSearchReward";
        targetAgent->printReward(ss.str(), outputFileTag); //write this file name with the tag
        ss.str("");
        ss << "sourceSearchPolicies";
        sourceAgent->publishLocalPolicies(ss.str(), outputFileTag);
        ss.str("");
        ss << "targetSearchPolicies";
        targetAgent->publishLocalPolicies(ss.str(), outputFileTag);
        sourceAgent->clearReward();
        targetAgent->clearReward();
        std::cout << "Exploration\n";
        std::cout << "\tSource-Ended " << searchNumberOfRuns << " runs averaging " << ((double) searchSourceTotalTimeCounter / searchNumberOfRuns) << " steps with a max of " << searchSourceMax << " steps\n";
        std::cout << "\tTarget-Ended " << searchNumberOfRuns << " runs averaging " << ((double) searchTargetTotalTimeCounter / searchNumberOfRuns) << " steps with a max of " << searchTargetMax << " steps\n";
        //exploitation
        //source
        sourceAgent->changeActionSelectionTemperature(1);
        int useSourcePoleCurrentAction = -10;
        int useSourceThisTimeCounter = 0;
        int useSourceTotalTimeCounter = 0;
        int useSourceMax = -1;
        //target
        targetAgent->changeActionSelectionTemperature(1);
        int useTargetPoleCurrentAction = -10;
        int useTargetThisTimeCounter = 0;
        int useTargetTotalTimeCounter = 0;
        int useTargetMax = -1;
        for (int a = 0; a < useNumberOfRuns; a++)
        {//run a few times
            bool sourceDone = false;
            while ((loopBreak == false)&&((sourceDone == false) || (targetCP->getCurrentState() != "-1")))
            {//while one is vertical 
                //source
                if (sourceDone != true)
                {
                    searchSourcePoleCurrentAction = atoi(sourceAgent->nominate().c_str());
                    sourceDone = sourceMC->executeAction(searchSourcePoleCurrentAction);
                    sourceAgent->updateLocal(sourceMC->getState());
                    sourceAgent->finishRun();
                    if (usingMergeAndAdapt == true)
                    {
                        sourceAgent->adaptAndReconfigure();
                    }
                    useSourceThisTimeCounter++;
                    useSourceTotalTimeCounter++; //the average counter
                }
                //target
                if (targetCP->getCurrentState() != "-1")
                {
                    useTargetPoleCurrentAction = atoi(targetAgent->nominate().c_str());
                    targetCP->executeAction(useTargetPoleCurrentAction);
                    targetAgent->updateLocal(targetCP->getCurrentState());
                    targetAgent->finishRun();
                    if (usingMergeAndAdapt == true)
                    {
                        targetAgent->adaptAndReconfigure();
                    }
                    useTargetThisTimeCounter++;
                    useTargetTotalTimeCounter++; //the average counter
                }
                if ((useTargetThisTimeCounter > maxTime) || (useSourceThisTimeCounter > maxTime))
                {//if we've been alive a very long time
                    loopBreak = true;
                    std::cout << "had to break because useTargetThisTimeCounter= " << useTargetThisTimeCounter << " and useSourceThisTimeCounter= " << useSourceThisTimeCounter << "\n";
                }

            }
            //std::cout << "Run " << a << " lasted for " << useThisTimeCounter << " steps\n";


            sourceMC->reset(false);
            targetCP->reset();
            loopBreak = false;
            if (useSourceThisTimeCounter > useSourceMax)
            {//if better than the best
                useSourceMax = useSourceThisTimeCounter;
            }
            sourceUse.push_back(useSourceThisTimeCounter);
            useSourceThisTimeCounter = 0;
            if (useTargetThisTimeCounter > useTargetMax)
            {//if better than the best
                useTargetMax = useTargetThisTimeCounter;
            }
            targetUse.push_back(useTargetThisTimeCounter);
            useTargetThisTimeCounter = 0;

        }
        std::cout << "Exploitation\n";
        std::cout << "\tSource-Ended " << useNumberOfRuns << " runs averaging " << ((double) useSourceTotalTimeCounter / useNumberOfRuns) << " steps with a max of " << useSourceMax << " steps\n";
        std::cout << "\tTarget-Ended " << useNumberOfRuns << " runs averaging " << ((double) useTargetTotalTimeCounter / useNumberOfRuns) << " steps with a max of " << useTargetMax << " steps\n";

        ss.str("");
        ss << "sourceUseReward";
        sourceAgent->printReward(ss.str(), outputFileTag); //write this file name with the tag
        ss.str("");
        ss << "targetUseReward";
        targetAgent->printReward(ss.str(), outputFileTag); //write this file name with the tag
        ss.str("");
        ss << "sourceUsePolicies";
        sourceAgent->publishLocalPolicies(ss.str(), outputFileTag);
        ss.str("");
        ss << "targetUsePolicies";
        targetAgent->publishLocalPolicies(ss.str(), outputFileTag);
        if (useSourceMax > useTargetMax)
        {//sourcebetter
            sourceBetterMax++;
        }
        if (useSourceTotalTimeCounter > useTargetTotalTimeCounter)
        {//sourcebetter
            sourceBetterAve++;
        }


        std::stringstream ss1;
        ss1 << "C://Users//CakeBox//Documents//NetBeansProjects//aamasResults//" << outputFileTag << "//" << mainLoopCount << "//";
        std::stringstream ss2;
        ss2 << "C://Users//CakeBox//Documents//NetBeansProjects//AMAAS-master//";
        TLmainWriteData(outputFilePrefix, outputFileTag);
        TLmainRunJava(ss1.str(), outputFileTag, ss2.str());
        std::cout << "End\n";
        std::cout << "\tAverages-Source was better in " << sourceBetterAve << " of " << bigLoop << "\n";
        std::cout << "\tMaximums-Source was better in " << sourceBetterMax << " of " << bigLoop << "\n";
        sourceSearch.clear();
        targetSearch.clear();
        sourceUse.clear();
        targetUse.clear();
    }
    return 0;
}

/*
 * write out data first is just a name tag, second is for identifying files to move
 */
void TLmainWriteData(std::string fileName, std::string tag)
{

    //print finshes
    std::stringstream ss4;
    ss4.str("");
    ss4 << fileName << "+sourceSearchSteps.csv." << tag << ".stats";
    ofstream fout3(ss4.str().c_str());
    std::vector<int>::iterator stepsIterator = sourceSearch.begin();
    while (stepsIterator != sourceSearch.end())
    {//for all steps

        fout3 << (*stepsIterator++) << ",";
    }
    fout3 << "\r\n";
    fout3.close();


    //print finshes
    std::stringstream ss4a;
    ss4a.str("");
    ss4a << fileName << "+targetSearchSteps.csv." << tag << ".stats";
    ofstream fout4a(ss4a.str().c_str());
    stepsIterator = targetSearch.begin();
    while (stepsIterator != targetSearch.end())
    {//for all steps

        fout4a << (*stepsIterator++) << ",";
    }
    fout4a << "\r\n";
    fout4a.close();


    //print finshes
    std::stringstream ss1;
    ss1.str("");
    ss1 << fileName << "+sourceUseSteps.csv." << tag << ".stats";
    ofstream fout1(ss1.str().c_str());
    stepsIterator = sourceUse.begin();
    while (stepsIterator != sourceUse.end())
    {//for all steps

        fout1 << (*stepsIterator++) << ",";
    }
    fout1 << "\r\n";
    fout1.close();


    //print finshes
    std::stringstream ss2;
    ss2.str("");
    ss2 << fileName << "+targetUseSteps.csv." << tag << ".stats";
    ofstream fout2(ss2.str().c_str());
    stepsIterator = targetUse.begin();
    while (stepsIterator != targetUse.end())
    {//for all steps

        fout2 << (*stepsIterator++) << ",";
    }
    fout2 << "\r\n";
    fout2.close();


}

/**
 * run a java prog to clean up where to put them and which to put there
 */
void TLmainRunJava(std::string location, std::string tag, std::string from)
{
    cout << "~~before java" << endl;
    std::stringstream ss;
    ss << "java -jar C://Users//CakeBox//Documents//NetBeansProjects//AMAAS-master//AAMASClean.jar ";
    ss << location;
    ss << " ";
    ss << tag;
    ss << " ";
    ss << from;
    system(ss.str().c_str());

    cout << "~~done java" << endl;

}