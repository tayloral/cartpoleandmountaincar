/* 
 * File:   Cusum.h
 * Author: Adam
 *
 * Created on 05 May 2015, 11:16
 */

#include <vector>
#ifndef CUSUM_H
#define	CUSUM_H

class Cusum
{
public:
    Cusum* deepCopy();
    Cusum();
    Cusum(double a, double b, double d);
    virtual ~Cusum();
    void SetModelBuilt(bool modelBuilt);
    bool IsModelBuilt() const;
    void SetSampleCount(int sampleCount);
    int GetSampleCount() const;
    void SetStdDev(double stdDev);
    double GetStdDev() const;
    void SetMean(double mean);
    double GetMean() const;
    int calculateEnvironmnetState(double sample);
    void SetDelta(double delta);
    double GetDelta() const;
    void SetBeta(double beta);
    double GetBeta() const;
    void SetAlpha(double alpha);
    double GetAlpha() const;
    double GetK() const;
    double GetH() const;
    void calculateHAndK();
    double GetSlo() const;
    double GetShi() const;
    int getLastOutput() const;
    void resetModel();
	bool getWasOutOfRange();
private:

    double mean;
    double stdDev;
    //sugested from montgomery k to be half the delta shift (0.5 in our example) and h to be around 4 or 5.
    double k;
    double h;
    double alpha; //probabliity of false alarm
    double beta; //the probability of not detecting that a shift in the process mean has, in fact, occurred
    double delta; //the amount of shift in the process mean that we wish to detect, expressed as a multiple of the standard deviation
    double shi; //the current high bar
    double slo; //current low bar
    int sampleCount;
    bool modelBuilt;
    int modelTime;
    std::vector<double> modelSamples;
    int lastOutput; //what we last calced
	bool wasOutOfRange;
};

#endif	/* CUSUM_H */

