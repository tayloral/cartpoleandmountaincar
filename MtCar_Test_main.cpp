/* 
 * File:   test_main.cpp
 * Author: Adam
 *
 * Created on 26 March 2015, 10:19
 */

#include <cstdlib>
#include <iostream>
#include "MtCar.h"
#include "DWL/include/DWLAgent.h"
#include "MtCarAgent.h"

using namespace std;

bool usingMergeAndAdapt = false;

/*
 * 
 */
int main(int argc, char** argv)//learning_test_//relocation error if no main
{
    cout << "mt car test\n";
    double explore[11 * 11];
    int index = -1;
    double exploite[11 * 11];
    for (double alpha = 0.0; alpha <= 1; alpha = alpha + .1)
    {
        cout << alpha << "-";
        cout.flush();
        for (double gamma = 0.0; gamma <= 1; gamma = gamma + .1)
        {
            cout << gamma << ",";
            cout.flush();
            index++;
            double thisExplore = 0;
            double thisExploite = 0;
            for (int times = 0; times < 10; times++)
            {
                //create and init an agent
                MtCar* mc = new MtCar();
                MtCarAgent* agent = new MtCarAgent("testAgent");
                agent->changeAlphaGamma(alpha, gamma);
                agent->changeActionSelectionTemperature(1000);
                agent->setUsingTransferLearning(false);
                agent->manageLearning(true, !true);
                agent->chooseActionSelectionMethod(false, false, !false);
                //storage space
                int searchPoleCurrentAction = -10;
                int searchThisTimeCounter = 0;
                int searchNumberOfRuns = 100;
                int searchTotalTimeCounter = 0;
                int searchMax = -1;

                for (int a = 0; a < searchNumberOfRuns; a++)
                {//run a few times
                    bool carFinished = false;
                    int heartbeat = 0; //moved heartbeat inside as we where miss counting
                    while (carFinished == false && heartbeat < 5000)
                    {//while vertical
                        searchPoleCurrentAction = atoi(agent->nominate().c_str());
                        bool actionResult = mc->executeAction(searchPoleCurrentAction);
                        if (actionResult == true)
                        {
                            carFinished = true;
                        }
                        agent->updateLocal(mc->getState());
                        agent->finishRun();
                        if (usingMergeAndAdapt == true)
                        {
                            agent->adaptAndReconfigure();
                        }
                        searchThisTimeCounter++;
                        searchTotalTimeCounter++; //the average counter
                        heartbeat++;
                        //std::cerr << "heartbeat= " << heartbeat << "\n";
                    }
                    //std::cout << "Run " << a << " lasted for " << searchThisTimeCounter << " steps\n";

                    agent->changeActionSelectionTemperature(1000 - (a * 1000 / searchNumberOfRuns)); //make a more directed search
                    mc->reset(false);
                    if (searchThisTimeCounter > searchMax)
                    {//if better than the best
                        searchMax = searchThisTimeCounter;
                    }
                    searchThisTimeCounter = 0;
                }
                std::cout << "Ended Exploration " << searchNumberOfRuns << " runs averaging " << ((double) searchTotalTimeCounter / searchNumberOfRuns) << " steps with a max of " << searchMax << " steps\n";
                thisExplore += ((double) searchTotalTimeCounter / searchNumberOfRuns);
                //exploitation
                agent->changeActionSelectionTemperature(1);
                int usePoleCurrentAction = -10;
                int useThisTimeCounter = 0;
                int useNumberOfRuns = 10;
                int useTotalTimeCounter = 0;
                int useMax = -1;

                for (int a = 0; a < useNumberOfRuns; a++)
                {//run a few times
                    bool carFinished = false;
                    int heartbeat = 0; //moved heartbeat inside as we where miss counting
                    while (carFinished == false && heartbeat < 5000)
                    {//while vertical
                        usePoleCurrentAction = atoi(agent->nominate().c_str());
                        bool actionResult = mc->executeAction(searchPoleCurrentAction);
                        if (actionResult == true)
                        {
                            carFinished = true;
                        }
                        agent->updateLocal(mc->getState());
                        agent->finishRun();
                        if (usingMergeAndAdapt == true)
                        {
                            agent->adaptAndReconfigure();
                        }
                        useThisTimeCounter++;
                        useTotalTimeCounter++; //the average counter
                        heartbeat++;
                    }
                    //std::cout << "Run " << a << " lasted for " << useThisTimeCounter << " steps\n";
                    mc->reset(false);
                    if (useThisTimeCounter > useMax)
                    {//if better than the best
                        useMax = useThisTimeCounter;
                    }
                    useThisTimeCounter = 0;
                }
                std::cout << "Ended Exploitation " << useNumberOfRuns << " runs averaging " << ((double) useTotalTimeCounter / useNumberOfRuns) << " steps with a max of " << useMax << " steps\n";
                thisExploite += ((double) useTotalTimeCounter / useNumberOfRuns);
            }
            explore[index] = thisExplore / 100;
            exploite[index] = thisExploite / 100;
        }
        cout << endl;
    }
    cout << "explore:\n";
    for (int a = 0; a < 11 * 11; a++)
    {
        cout << explore[a] << ",";
    }
    cout << "\nexploite:\n";
    for (int a = 0; a < 11 * 11; a++)
    {
        cout << exploite[a] << ",";
    }
    return 0;


}